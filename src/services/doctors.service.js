import { $algolia } from "../api/algolia.service";
import { DoctorModel } from '@/models/doctor.model'

let instance = null

export class DoctorsService {

    constructor () {
        if (!instance) {
            instance = this
        }
        return instance
    }

    async searchDoctors ({ query, hitsPerPage = 4 }) {
        try {
            const { hits } =  await $algolia.post('pl_autocomplete_doctor/query', { params: `query=${query}&hitsPerPage=${hitsPerPage}`});
            return DoctorModel.fromJsonArray(hits || [])
        } catch (err) {
            throw err
        }
    }

}

export default new DoctorsService()