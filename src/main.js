import Vue from 'vue'
import Configurator from './Configurator.vue'

import './plugins'
import './styles'
import './sharedComponents'
Vue.config.productionTip = false

new Vue({
  render: h => h(Configurator)
}).$mount('#app')
