import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify, {
    theme: {
        primary: '#4185dc'
    }
})
