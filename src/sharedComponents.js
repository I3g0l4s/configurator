import Vue from 'vue'
import LoaderIcon from '@/components/LoaderIcon'

Vue.component('zn-loader-icon', LoaderIcon)