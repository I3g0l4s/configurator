import axios from 'axios';
import * as join from 'url-join';

export default class AxiosService {

    constructor({url, apiKey, appID}) {
        this.apiUrl = url;
        this.apiKey = apiKey;
        this.appID = appID
    }

    get(path) {
        return axios.get(join(this.apiUrl, path))
    }

    post(path, payload) {
        return axios.request({
            method: 'POST',
            url: join(this.apiUrl, path),
            responseType: 'json',
            data: {
                ...payload,
                apiKey: this.apiKey,
                appID: this.appID,
            },
        }).then(response => response.data)
          .catch(e =>  {
              throw e
          })
    }
}