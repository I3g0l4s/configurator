import AxiosService from "./axios.service";

let instance = null;

class AlgoliaService extends AxiosService {

    constructor() {
        super({
            url: 'https://docplanner-2.algolia.io/1/indexes/',
            apiKey: '90a529da12d7e81ae6c1fae029ed6c8f',
            appID: 'docplanner'
        });

        if (!instance) {
            instance = this
        }
        return instance
    }
}
export const $algolia = new AlgoliaService()

