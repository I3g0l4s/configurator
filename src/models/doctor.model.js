export class DoctorModel {

    static fromJsonArray(json) {
        return json.map(DoctorModel.fromJson)
    }

    static fromJson({   fullname_formatted,
                        image_micro_square,
                        name,
                        surname,
                        url,
                        urlname }) {
        return new DoctorModel(
            fullname_formatted,
            image_micro_square,
            name,
            surname,
            url,
            urlname
        )
    }

    constructor(fullname_formatted,
                image_micro_square,
                name,
                surname,
                url,
                urlname) {
            this.fullname_formatted = fullname_formatted,
            this.image_micro_square = image_micro_square,
            this.name = name,
            this.surname = surname,
            this.url = url,
            this.urlname = urlname

    }
}
