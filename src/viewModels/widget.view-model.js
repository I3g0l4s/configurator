class WidgetModel {
    constructor(name, hide_branding, opinion, type, height) {
        this.name = name;
        this.hide_branding = hide_branding;
        this.opinion = opinion;
        this.type = type;
        this.height = height
    }

    generateWidgetScript({ url, urlname, name, surname }) {
        return `<a id="zl-url" class="zl-url" href="${url}" rel="nofollow" data-zlw-doctor="${urlname}" data-zlw-type="${this.type}" data-zlw-opinion="${this.opinion}" data-zlw-hide-branding="${this.hide_branding}">${name} ${surname} - ZnanyLekarz.pl</a><script>!function($_x,_s,id){var js,fjs=$_x.getElementsByTagName(_s)[0];if(!$_x.getElementById(id)){js = $_x.createElement(_s);js.id = id;js.src = "//www.docplanner-platform.com/js/widget.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","zl-widget-s");</script>`

    }

    generateWidgetLink({ urlname }) {
        return {
            link: `//www.znanylekarz.pl/ajax/marketing/doctor/widget/big/${urlname}?hide_branding=${this.hide_branding}&opinion=${this.opinion}`,
            type: this.type,
            height: this.height
        }
    }
}

const widgets =  [
    new WidgetModel('Standardowy', true, false, 'big', 169),
    new WidgetModel('Standardowy z opinią', true, true, 'big_opinion', 269)
]

export default widgets