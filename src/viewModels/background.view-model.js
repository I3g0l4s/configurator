export class BackgroundViewModel {
    constructor(name, color, image) {
        this.name = name;
        this.color = color;
        this.image = image;
    }
}

const backgrounds = [
    new BackgroundViewModel('checker', 'transparent', 'https://www.docplanner-platform.com/img/general/layout/checker-bg.jpg'),
    new BackgroundViewModel('silver', '#eef2f2'),
    new BackgroundViewModel('dark', '#333333')
]

export default backgrounds